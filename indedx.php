<?php 
include  'Conexion/Procesos.php';

$ShowData = new Procesos();
$DatosPaginacion = $ShowData->BuscarPaginacion();


	$TotaldeCuentas = $DatosPaginacion->rowCount();
	$Cuentas_X_pagina = 6;
	$paginas = $TotaldeCuentas / 6;
	$paginas = ceil($paginas);
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Nell Perro</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
	<h1>Ejemplo</h1>
	<?php 
	if (!$_GET) {
		header('Location:index.php?pagina=1');
	}

	$iniLimit = ($_GET['pagina']-1) * $Cuentas_X_pagina;

	$sql_cuentas = 'SELECT * FROM cuentascatalogo LIMIT :iniLimit,:nCuentas';
	$sentencia_cuentas = $pdo->prepare($sql_cuentas);
	$sentencia_cuentas->bindParam(':iniLimit', $iniLimit, PDO::PARAM_INT);
	$sentencia_cuentas->bindParam(':nCuentas', $Cuentas_X_pagina, PDO::PARAM_INT);

	$sentencia_cuentas->execute();
	$resultado_cuentas = $sentencia_cuentas->fetchAll();

	 ?>
	<table class="table">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Codigo Cuenta</th>
      <th scope="col">Nombre Cuenta</th>
      <th scope="col">Saldo Actual</th>
      <th scope="col">Saldo Anterior</th>
    </tr>
  </thead>
  <tbody>
		<?php foreach ($resultado_cuentas as $cuentas): ?>
		
		    <tr>
		      <th scope="row"><?php echo $cuentas['idCuenta'] ?></th>
		      <td><?php echo $cuentas['CodigoCuenta'] ?></td>
		      <td><?php echo $cuentas['NombreCuenta'] ?></td>
		      <td><?php echo $cuentas['saldo_actual']; ?></td>
		      <td><?php echo $cuentas['saldo_anterior']; ?></td>
		    </tr>
		 
	<?php endforeach ?>
	 </tbody>
   </tbody>
</table>
	<nav aria-label="Page navigation example">
          <ul class="pagination justify-content-center">
            <li class="page-item <?php echo $_GET['pagina']<=1? 'disabled': '' ?>">
            <a class="page-link" href="index.php?pagina= <?php echo $_GET['pagina']-1 ?>">Anterior</a>
          </li>

            <?php for($i=0; $i<$paginas; $i++): ?>
              <li class="page-item <?php echo $_GET['pagina']==$i+1 ? 'active' : '' ?>">

                <a class="page-link" href="index.php?pagina= <?php echo $i+1 ?>"><?php echo $i+1; ?></a>

              </li>
            <?php
              endfor
             ?>

            <li class="page-item <?php echo $_GET['pagina']>=$paginas? 'disabled': '' ?>">
              <a class="page-link" href="index.php?pagina= <?php echo $_GET['pagina']+1 ?>">Siguiente</a>
            </li>
          </ul>
        </nav>
</body>
</html>