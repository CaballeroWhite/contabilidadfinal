<?php

include ('../sidebar.php');
//include '../inc/Procesos.php';
?>
<!DOCTYPE html>

<html lang="en" dir="ltr"> 

    <head>
        <meta charset="utf-8">
        <link href="../assets/css/CustomCss.css" rel="stylesheet">
        <title></title>
    </head>

    <body>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <i class="material-icons">
                <a href="#">arrow_back_ios</a>
            </i>
            <a class="navbar-brand" href="#">Registro de datos</a>
          </div>

          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
              </div>
            </form>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  
                  <div>
                    <h4 class="card-title ">Tabla Gasto</h4>
                    <p class="card-category"> Datos de la tabla Gasto</p>
                    <br>
                    <i class="material-icons">
                      <a id="md-Border" href="N_Gastos.php">add</a>
                    </i>
                    <p class="card-category">Nuevo consulta de gasto</p>
                  </div>
                </div>

                <form method="POST">
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">group</i>
                            </span>
                        </div>

                            <input class="form-control" type="text" name="id_gastos" placeholder="buscar por id">
                    </div>
                    </div>
                    <center>
                    <input class="btn btn-primary" type="submit" name="listar" value="Mostrar Todo">
                    <input class="btn btn-info" type="submit" name="Buscar" value="Buscar">
                    </center>
                   
                </form>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>ID</th>
                         <th>DECRIPCION</th>
                         <th>CANTIDAD</th>
                         <th>Cuenta</th>
                         <th>CATEGORIA</th>
                        <th>FECHA</th>
                       
                        
                        
                       
                        
                      </thead>
                      <tbody>
                        <tr>
                        <?php
            
            include '../Conexion/Procesos.php';


            if(isset($_POST['Buscar'])){

                $gastos = new Procesos();
                $DatosB = $gastos->BuscarDatos($_POST['id_gastos'],"id_gastos","gastos");


                if(!$DatosB){
                    echo "Datos no encontrados";
                }else{

                    foreach($DatosB as $fila){
                        echo "<tr>";
                        echo "<td>".$fila['id_gastos']."</td>";
                        echo "<td>".$fila['descripcion']."</td>";
                        echo "<td>".$fila['cantidad']."</td>";
                         echo "<td>".$fila['Cuenta']."</td>";
                        echo "<td>".$fila['categoria']."</td>";
                        echo "<td>".$fila['fecha']."</td>";
                        //valores en eliminar tabla = nombre de la tabla ejempo -> locales_medicos
                        // idCampo = el nombre del campo del id en la tabla ejemplo -> id_Local
                        // id= al campo en la tabla ejemplo -> id_local
        
                        echo "<td><a href='Eliminar.php?tabla=gastos&idCampo=id_gastos&id_gastos=".$fila['id_gastos']."'><i class='material-icons'>delete_sweep</i></a></td>";
                        echo "<td><a href='Formulario_Modificar.php?idD=".$fila['id_gastos']."'><i class='material-icons'>update</i></a></td>";
                        echo "</tr>";

    
    
                    }

                }


            }else if(isset($_POST['listar'])){
                Listar();
            }else{
                Listar();
            }

            function Listar(){
                $gastos = new Procesos();
                $Campos = array(
                     "id_gastos",
                     "descripcion",
                    "cantidad",
                    "Cuenta",
                    "categoria",
                    "fecha"
                );
                $Datos = $gastos->Consulta($Campos,"gastos");

                foreach($Datos as $fila){
                    echo "<tr>";
                    echo "<td>".$fila['id_gastos']."</td>";
                     echo "<td>".$fila['descripcion']."</td>";
                     echo "<td>".$fila['cantidad']."</td>";
                     echo "<td>".$fila['Cuenta']."</td>";
                     echo "<td>".$fila['categoria']."</td>";
                    echo "<td>".$fila['fecha']."</td>";
                   
                    
                    
                    //valores en eliminar tabla = nombre de la tabla ejempo -> locales_medicos
                    // idCampo = el nombre del campo del id en la tabla ejemplo -> id_Local
                    // id= al campo en la tabla ejemplo -> id_local
    
                    //echo "<td>eliminar</td>";
                    //echo "<td>actualizar</td>";
                    //echo "</tr>";
                    echo "<td><a href='Eliminar.php?tabla=gastos&idCampo=id_gastos&id_gastos=".$fila['id_gastos']."'><i class='material-icons'>delete_sweep</i></a></td>";
                        echo "<td><a href='Formulario_Modificar.php?idD=".$fila['id_gastos']."'><i class='material-icons'>update</i></a></td>";
                        
                        echo "</tr>";


                }

            }

            
            
            ?>
        </table>


    </body>

</html>