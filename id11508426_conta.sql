-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 06-12-2019 a las 00:33:36
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `id11508426_conta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_cuenta`
--

CREATE TABLE `categoria_cuenta` (
  `activo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasivo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `patrimonio` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ingresos` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gastos` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `liquidación` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_gastos`
--

CREATE TABLE `categoria_gastos` (
  `id_categoria` int(11) NOT NULL,
  `nombre` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `categoria_gastos`
--

INSERT INTO `categoria_gastos` (`id_categoria`, `nombre`, `fecha`) VALUES
(1, 'telefono', '2019-12-11'),
(2, 'equipo tecnologico', '2019-12-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_ingreso`
--

CREATE TABLE `categoria_ingreso` (
  `id_categoria` int(11) NOT NULL,
  `nombre` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `categoria_ingreso`
--

INSERT INTO `categoria_ingreso` (`id_categoria`, `nombre`, `fecha`) VALUES
(1, 'Telefono', '2019-11-07'),
(2, 'ALV', '2019-11-07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentascatalogo`
--

CREATE TABLE `cuentascatalogo` (
  `idCuenta` int(11) NOT NULL,
  `CodigoCuenta` int(10) DEFAULT NULL,
  `NombreCuenta` varchar(60) DEFAULT NULL,
  `tipo_movimiento` varchar(10) DEFAULT NULL,
  `saldo_anterior` float DEFAULT NULL,
  `saldo_actual` float DEFAULT NULL,
  `cargos` float DEFAULT NULL,
  `abonos` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuentascatalogo`
--

INSERT INTO `cuentascatalogo` (`idCuenta`, `CodigoCuenta`, `NombreCuenta`, `tipo_movimiento`, `saldo_anterior`, `saldo_actual`, `cargos`, `abonos`) VALUES
(1, 1101, 'Efectivo y Equivalente', NULL, NULL, 200, 0, 900),
(2, 1102, 'Inventario', NULL, NULL, NULL, 0, 0),
(3, 1103, 'Clientes y Otras Cuentas por Cobrar', NULL, NULL, 1200, 300, 0),
(4, 1104, 'Inversiones Financieras', NULL, NULL, 2000, NULL, NULL),
(5, 1105, 'Impuestos Corrientes por Recuperar', NULL, NULL, NULL, NULL, NULL),
(6, 1106, 'Pagos Anticipados', NULL, NULL, NULL, NULL, NULL),
(7, 1201, 'Propiedad Planta y Equipo', NULL, NULL, NULL, NULL, NULL),
(8, 1202, 'Depreciacion Acumulada Propiedad Planta y Equipo', NULL, NULL, NULL, NULL, NULL),
(9, 1203, 'Propiedades en Inversion', NULL, NULL, 1000, NULL, NULL),
(10, 1204, 'Intangibles', NULL, NULL, NULL, NULL, NULL),
(11, 1205, 'Amortizacion de Activos Intangibles', NULL, NULL, NULL, NULL, NULL),
(12, 1206, 'Cuentas por Cobrar a Largo Plazo', NULL, NULL, NULL, NULL, NULL),
(13, 1207, 'Depositos en Garantia', NULL, NULL, NULL, NULL, NULL),
(14, 1208, 'Inversiones Permanentes', NULL, NULL, NULL, NULL, NULL),
(15, 1209, 'Impuestos Sobre la Renta Diferido', NULL, NULL, NULL, NULL, NULL),
(16, 2101, 'Deudas Comerciales y Otras Cuentas por Pagar', NULL, NULL, NULL, NULL, NULL),
(17, 2102, 'Pasivos Financieros a Corto Plazo', NULL, NULL, NULL, NULL, NULL),
(18, 2103, 'Impuestos corrientes por pagar', NULL, NULL, NULL, NULL, NULL),
(19, 2104, 'Beneficios a Empleados', NULL, NULL, NULL, NULL, NULL),
(20, 2105, 'Obligaciones por Arrendamiento Financiero', NULL, NULL, NULL, NULL, NULL),
(21, 2106, 'Retenciones a Empleados', NULL, NULL, NULL, NULL, NULL),
(22, 2107, 'Provisiones', NULL, NULL, NULL, NULL, NULL),
(23, 2201, 'Prestamos por Pagar a Largo Plazo', NULL, NULL, NULL, NULL, NULL),
(24, 2202, 'Beneficios a los Empleados', NULL, NULL, NULL, NULL, NULL),
(25, 2203, 'Pasivos por Impuestos Diferidos', NULL, NULL, NULL, NULL, NULL),
(26, 2204, 'Obligaciones por Arrendamientos Financieros', NULL, NULL, NULL, NULL, NULL),
(27, 31, 'Capital', NULL, NULL, NULL, NULL, NULL),
(28, 3103, 'Resultados del Ejercicio', NULL, NULL, NULL, NULL, NULL),
(29, 4101, 'Costos de Ventas', NULL, NULL, NULL, NULL, NULL),
(30, 4102, 'Compras', NULL, NULL, NULL, NULL, NULL),
(31, 4103, 'Rebajas y Devoluciones sobre ventas', NULL, NULL, NULL, NULL, NULL),
(32, 4201, 'Gastos de Ventas y Comercializacion', NULL, NULL, NULL, NULL, NULL),
(33, 4202, 'Gastos de Administracion', NULL, NULL, NULL, NULL, NULL),
(34, 4202, 'Gastos por Impuestos a las Ganancias', NULL, NULL, NULL, NULL, NULL),
(35, 4205, 'Gastos Financieros', NULL, NULL, NULL, NULL, NULL),
(36, 5101, 'Ingresos por Ventas', NULL, NULL, NULL, NULL, NULL),
(37, 5102, 'Otros Ingresos', NULL, NULL, NULL, NULL, NULL),
(38, 5201, ' Ingresos Financieros ', NULL, NULL, NULL, NULL, NULL),
(39, 6101, 'Perdidas o Ganancias', NULL, NULL, NULL, NULL, NULL),
(40, 7101, 'Cuentas de Orden Deudoras', NULL, NULL, NULL, NULL, NULL),
(41, 7102, 'Cuentas de Orden Acreedoras', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `id_gastos` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantidad` float DEFAULT NULL,
  `categoria` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`id_gastos`, `descripcion`, `cantidad`, `categoria`, `fecha`) VALUES
(1, 'DDD', 120, 'DDD', '2019-11-20'),
(2, 'AAA', 500, 'ddd', '2019-11-20'),
(3, 'BBB', 1000, 'sss', '2019-11-20'),
(4, 'CCC', 1500, 'ssss', '2019-11-20'),
(5, 'DDD', 200, 'ggg', '2019-11-20'),
(6, 'nada', 500, 'telefono', '2019-12-17'),
(7, 'nada', 500, 'equipo tecnologico', '2019-12-10'),
(8, 'nn', 500, 'telefono', '2019-12-11'),
(9, 'nn', 500, 'telefono', '2019-12-11'),
(10, 'nada', 500, 'equipo tecnologico', '2019-12-11'),
(11, 'Dd', 200, 'telefono', '2019-12-18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingresos`
--

CREATE TABLE `ingresos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `categoria` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `cantidad` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ingresos`
--

INSERT INTO `ingresos` (`id`, `descripcion`, `fecha`, `categoria`, `cantidad`) VALUES
(1, 'AAA', '2019-11-20', 'ALV', 1000),
(2, 'DDD', '2019-11-13', 'Telefono', 5000),
(4, '123', '2019-11-13', 'Telefono', 1000),
(5, 'Pago de sueldo', '2019-11-13', 'Telefono', 500),
(6, 'Sorteo', '2019-11-06', 'ALV', 100),
(7, 'fff', '2019-11-13', 'Telefono', 1000),
(8, 'v', '2019-11-05', 'Telefono', 500),
(9, 'fff', '2019-11-13', 'Telefono', 500),
(10, 'Lavado', '2019-11-06', 'Telefono', 10000),
(11, 'Carwash Negocio', '2019-11-13', 'Telefono', 500),
(12, 'Pago', '2019-11-20', 'Telefono', 500),
(13, 'Cobro cuenta', '2019-11-12', 'Telefono', 100),
(14, 'cobro cuenta', '2019-11-13', 'Telefono', 100),
(15, 'nuevo', '2019-12-27', 'Telefono', 455),
(16, 'nn', '2019-12-04', 'ALV', 455),
(17, 'nada', '2019-12-31', 'ALV', 1999),
(18, 'D', '2018-11-03', 'Telefono', 100),
(19, 'Venta Inmueble', '2019-12-10', 'Telefono', 500),
(20, 'Ventas', '2019-12-03', 'Telefono', 600),
(21, 'ddd', '2019-12-03', 'Telefono', 100),
(22, 'fghb', '2019-10-08', 'Telefono', 1000),
(23, 'fghj', '2019-12-02', 'Telefono', 1000),
(24, 'fghjl45', '2019-12-02', 'Telefono', 1000),
(25, 'Producto', '2019-12-10', 'Telefono', 200);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_partidas`
--

CREATE TABLE `t_partidas` (
  `idPartida` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `NPartida` varchar(45) NOT NULL,
  `Cuenta` varchar(45) NOT NULL,
  `Concepto` varchar(45) NOT NULL,
  `Debe` float NOT NULL,
  `Haber` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_partidas`
--

INSERT INTO `t_partidas` (`idPartida`, `Fecha`, `NPartida`, `Cuenta`, `Concepto`, `Debe`, `Haber`) VALUES
(1, '2019-12-01', '1', '1101', 'Efectivo y Equivalente', 200, 0),
(2, '2019-12-01', '2', '1103', 'Clientes y Otras Cuentas por Cobrar', 0, 200),
(3, '2019-12-02', '1', '1101', 'Efectivo y Equivalente', 100, 0),
(4, '2019-12-02', '1', '1101', 'Efectivo y Equivalente', 100, 0),
(5, '2019-12-06', '2', '', '', 0, 0),
(6, '2019-12-06', '2', '', '', 0, 0),
(7, '2019-12-06', '1', '1101', 'Efectivo y Equivalente', 100, 0),
(8, '2019-12-06', '2', '1103', 'Clientes y Otras Cuentas por Cobrar', 0, 200),
(9, '2019-12-07', '1', '1102', 'Inventario', 300, 0),
(10, '2019-12-07', '2', '1101', 'Efectivo y Equivalente', 0, 300),
(11, '2019-12-05', '1', '1101', 'Efectivo y Equivalente', 100, 0),
(12, '2019-12-05', '2', '1102', 'Inventario', 0, 100),
(13, '2019-12-05', '1', '1101', 'Efectivo y Equivalente', 100, 0),
(14, '2019-12-05', '2', '1102', 'Inventario', 0, 100),
(15, '2019-12-11', '1', '+', '', 0, 0),
(16, '2019-12-11', '2', '', '', 0, 0),
(17, '2019-12-18', '1', '1101', 'Efectivo y Equivalente', 100, 0),
(18, '2019-12-18', '2', '1103', 'Clientes y Otras Cuentas por Cobrar', 0, 100),
(19, '2019-12-06', '1', '1101', 'Efectivo y Equivalente', 100, 0),
(20, '2019-12-06', '2', '1102', 'Inventario', 0, 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `nombre_usuario` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PASSWORD` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombre_usuario`, `PASSWORD`) VALUES
(1, 'luis ', 'MTIzNDU2'),
(2, 'luis ', 'MTIzNDU2'),
(3, 'Luis', 'MTIzNDU2'),
(4, '', ''),
(5, '', ''),
(6, 'Luchoportuano', 'MTIzNDU2'),
(7, 'manberroi', 'MTIzNDU2'),
(8, 'afas', 'MTIzNDU2');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria_gastos`
--
ALTER TABLE `categoria_gastos`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `categoria_ingreso`
--
ALTER TABLE `categoria_ingreso`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `cuentascatalogo`
--
ALTER TABLE `cuentascatalogo`
  ADD PRIMARY KEY (`idCuenta`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id_gastos`);

--
-- Indices de la tabla `ingresos`
--
ALTER TABLE `ingresos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `t_partidas`
--
ALTER TABLE `t_partidas`
  ADD PRIMARY KEY (`idPartida`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria_ingreso`
--
ALTER TABLE `categoria_ingreso`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cuentascatalogo`
--
ALTER TABLE `cuentascatalogo`
  MODIFY `idCuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `id_gastos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `ingresos`
--
ALTER TABLE `ingresos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `t_partidas`
--
ALTER TABLE `t_partidas`
  MODIFY `idPartida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
