
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php


?>
<!DOCTYPE HTML>
<html>
<head>
<title>Sistema De Contabilidad</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/sb-admin-2.min.css" rel="stylesheet">

<link href="css/font-awesome.css" rel="stylesheet"> 
<script src="js/jquery.min.js"> </script>
<script src="js/bootstrap.min.js"> </script>
</head>
<body>
	<div class="login">
	    <h1 class="h4 text-gray-900 mb-4">Bienvenid@!</h1>
	    <h1 class="h4 text-gray-900 mb-4">al</h1>
        
		<h1 class="h4 text-gray-900 mb-4"><a href="index.php"> Sistema de Contabilidad</a></h1>
		<div class="login-bottom">
			<h2>Login</h2>
			<form method="POST" action="../Login/sesion.php">
			<div class="col-md-6">
				<div class="login-mail">
					<input type="text" name="UserN" placeholder="Usuario" required="campo Requerido">
					<i class=""></i>
				</div>
				<div class="login-mail">
					<input type="password" name="PassW" placeholder="Password" required="campo requerido">
					<i class=""></i>
				</div>
				   

			
			</div>
			<div class="col-md-6 login-do">
				<label class="hvr-shutter-in-horizontal login-sub">
					<input type="submit" value="login" class="btn">
					</label>
				
			</div>
			
			<div class="clearfix"> </div>
			</form>
		</div>
	</div>
		<!---->

<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	  <!-- Bootstrap core JavaScript-->

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
	<!--//scrolling js-->
</body>
</html>

