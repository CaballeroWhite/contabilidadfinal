<?php

session_start();
if(!isset($_SESSION['user'])){
    header('location:../index.php');
}

?>

<!doctype html>
<html lang="en">
<head>
  <title>Registro</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
  <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
</head>

<body class="dark-edition">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="black" data-image="./assets/img/sidebar-2.jpg">

      <div class="logo">
        <a href="index.php" class="simple-text logo-normal">
          Registro Contable
        </a>
      </div>
   
      <div class="sidebar-wrapper">
        <ul class="nav">
 <li class="nav-item active  ">
            <a class="nav-link" href="../Login/Actualizar.php">
              <i class="material-icons">person</i>
              <p>Esta logeado como:</p>
						
							<?php 
							
							echo $_SESSION['user'];
							?>
							</i>
            </a>
    </li>            
          <li class="nav-item active  ">
            <a class="nav-link" href="home.php">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <!-- your sidebar here -->
          <li class="nav-item active  ">
            <a class="nav-link" href="../T_NPartida/Tabla_Partida.php">
              <i class="material-icons">dynamic_feed</i>
              <p>Partidas</p>
            </a>
          </li>
          <li class="nav-item active  ">
            <a class="nav-link" href="../T_Cuentas/Tabla_Cuentas.php">
              <i class="material-icons">payment</i>
              <p>Cuentas</p>
            </a>
          </li>
          <li class="nav-item active  ">
            <a class="nav-link" href="../T_Ingresos/Tabla_Ingresos.php">
              <i class="material-icons">exit_to_app</i>
              <p>Ingresos</p>
            </a>
          </li>
          
          <li class="nav-item active  ">
            <a class="nav-link" href="../T_Gastos/Tabla_Gastos.php">
              <i class="material-icons">assignment_return</i>
              <p>Gastos</p>
            </a>
          </li>
          <li class="nav-item active  ">
            <a class="nav-link" href="../T_Categorias_Ingresos/T_Categorias_Ingresos.php">
              <i class="material-icons">picture_in_picture</i>
              <p>Categorias de ingresos</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="../T_Categorias_Gastos/T_Categorias_Gastos.php">
              <i class="material-icons">picture_in_picture_alt</i>
              <p>Categoria de gastos</p>
            </a>
          </li>

          <li class="nav-item active  ">
            <a class="nav-link" href="../nuevousuario.php">
              <i class="material-icons">person</i>
              <p>Registrar nueva cuenta</p>
              <a class="nav-link" href="../Login/CerrarSesion.php">
              <p>Cerrar sesion</p>
            </a>
          </li>
        </ul>
      </div>
    </div>

