<?php
include '../Conexion/Procesos.php';
$ingresos = new Procesos();
include '../sidebar.php';

$id= $_GET['idD'];
$CargarDatos = $ingresos->CargaDatos($id,"id","ingresos");


?>
<!DOCTYPE html>

<html>

    <head>
        <link href="../assets/css/CustomCss.css" rel="stylesheet">
        <title>Modificar ingresos</title>
    </head>
    <body>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top" id="navigation-example">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <i class="material-icons">
                <a href="Tabla_Ingresos.php">arrow_back_ios</a>
            </i>
            <a class="navbar-brand" href="Tabla_Ingresos.php">Ver registros</a>
          </div>

          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
              </div>
            </form>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  
                  <div>
                    <h4 class="card-title ">Actualizar datos</h4>
                  </div>
                </div>

                <div class="content">
                  <div class="col-md-12">
                    <form method="POST" action="IngresosUpdate.php">
                    <?php
    
                        foreach($CargarDatos as $Datos){

                    ?>
                      <div class="row">

                         <div class="col-md-6">
                            
                                <div class="form-group">

                                    <label for="Descripcion">Descripción</label>
                                    <textarea id="textDescripcionarea" name="Descripcion" class="form-control"><?php echo $Datos["descripcion"]?></textarea>
                                </div>
                        </div>

                        <div class="col-md-5">
                                <div class="form-group">
                                    <label for="cantidad">Cantidad</label>
                                    <input type="text" name="cantidad" class="form-control" value="<?php echo $Datos["cantidad"]?>">  
                                </div>
                        </div>

                      </div>

                      <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Category">Categoria</label>
                                    <select name='Category' class="form-control selectOP">
                                            <?php
                                            
                                                $Categoria = new Procesos();
                                                $Info = $Categoria->MostrarCategorias("nombre","categoria_ingreso");
                                                foreach($Info as $ingresos){
                                            ?>
                                                <option><?php echo $ingresos["nombre"];?></option>
                                            
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="fecha">Fecha correspondiente</label>
                                <input type="date" class="form-control"  name="fecha"  value="<?php echo $Datos["fecha"]?>" required>
                            </div>
                      </div>

                      </div>
                      <input type="hidden" name="id" value="<?php echo $id ?>">
                      <div class="col-md-12">
                            <input class="btn btn-primary btn-round" id="GuardarIngreso" type="submit" name="save" value="Guardar">
                            
                      </div> 

                      

                   
                    </form>

                    <?php } ?>





                                             </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





        
 

        <form method="POST" action="IngresosUpdate.php">

            <div id="Descp">
                
            </div>
            <div id="Cantidad">
                
            </div>
            <div id="Categoria">
                
            </div>
            <div id="fecha">
                
            </div>
            
            
        </form>



       

    </body>


</html>