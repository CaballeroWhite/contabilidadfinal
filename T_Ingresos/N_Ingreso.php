<?php
include '../Conexion/Procesos.php';
include 'AumentoCuenta.php';
include '../sidebar.php';
?>
<!DOCTYPE html>

<html>

    <head>
        <link href="../assets/css/CustomCss.css" rel="stylesheet">
        <title>Nuevo ingreso</title>
    </head>
    <body>

    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <i class="material-icons">
                <a href="Tabla_Ingresos.php">arrow_back_ios</a>
            </i>
            <a class="navbar-brand" href="#">Mostrar Registros</a>
          </div>

          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
              </div>
            </form>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  
                  <div>
                    <h4 class="card-title ">Nuevo registro a la tabla ingresos</h4>
                  </div>
                </div>

                <div class="content">
                  <div class="col-md-12">
                    <form method="POST">
                      <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="Descripcion">Descripción</label>
                                  <textarea id="textDescripcionarea" class="form-control" name="Descripcion"  rows="1" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-5">
                              <div class="form-group">
                                  <label for="cantidad">Cantidad</label>
                                  <input type="text" class="form-control" name="cantidad" required>  
                        
                              </div>
                            </div>
                      </div>

                      <div class="row">
                          <div class="col-md-4">
                                  <div class="form-group">
                                      <label for="Category">Categoria</label>
                                      <select class="form-control selectOP"  name='Category' >
                                          <option>--Seleccione la categoria--</option>
                                      <?php
                                      
                                          $Categoria = new Procesos();
                                          $Info = $Categoria->MostrarCategorias("nombre","categoria_ingreso");
                                          foreach($Info as $ingresos){
                                      ?>
                                          <option><?php echo $ingresos["nombre"];?></option>
                                          
                                      <?php } ?>

                                      </select>
                                  </div>
                          </div>

                          <div class="col-md-4">
                                  <div class="form-group">
                                          <label for="Cuenta">Cuenta</label>
                                          <select class="form-control selectOP"  name='Cuenta' >
                                              <option>--Seleccione la cuenta--</option>
                                          <?php
                                          
                                              $Cuentas = new Procesos();
                                              $Info = $Cuentas->MostrarCuentas("NombreCuenta","cuentascatalogo","CodigoCuenta");
                                              foreach($Info as $Cuenta){
                                          ?>
                                              <option value="<?php echo $Cuenta["NombreCuenta"];?>">Codigo -- <?php echo $Cuenta["CodigoCuenta"];?> Cuenta -- <?php echo $Cuenta["NombreCuenta"];?> </option>
                                              
                                          <?php } ?>

                                          </select>
                                      </div>
                          
                            </div>
                          

                          <div class="col-md-4">
                                  <div class="form-group">
                                      <label for="fecha">Fecha correspondiente</label>
                                      <input type="date" class="form-control"  name="fecha"  required>

                                  </div>
                          </div>
                      </div>
                            <div class="col-md-12">
                              <input class="btn btn-primary btn-round" id="GuardarIngreso" type="submit" name="save" value="Guardar">
                            </div>                
                      </form>

                    </div>

                  </div>
                    </div>
                </div>
              </div>
            </div>
      </div>
    </div>
    </body>


</html>

<?php




$Ingresos = new Procesos();
$CuentaUpdate = new Cuenta(); //Instancia objeto Cuenta en el archivo AumentoCuenta en esta misma carpeta

if(isset($_POST['save'])){

    $Desc = $_POST['Descripcion'];
    $Cant = $_POST['cantidad'];
    $Category = $_POST['Category'];
    $Fecha = $_POST['fecha'];

    $NuevoIngreso = $_POST['cantidad'];
    $Cuenta = $_POST['Cuenta'];

    $Campos = Array(
        "descripcion",
        "fecha",
        "categoria",
        "cantidad",
        "Cuenta"
    );

    $Valores = Array(
        $Desc,
        $Fecha,
        $Category,
        $Cant,
        $Cuenta
        
    );

    
    echo $Ingresos->Insertar($Campos,"ingresos",$Valores);
    
 //Proceso para actualizar el balance de los datos en las cuentas
    $GETBalanceInicial = $CuentaUpdate->ShowBalanceInicial("saldo_actual","cuentascatalogo","NombreCuenta",$Cuenta);
 
    foreach($GETBalanceInicial as $BI){
      echo $BI['saldo_actual'];
      $BalanceObtenido = ($BI['saldo_actual'] + $NuevoIngreso);
    }

    $BalanceFinalCuenta = $CuentaUpdate->ActualizarCuenta("cuentascatalogo","saldo_actual",$BalanceObtenido,"NombreCuenta",$Cuenta);
    
    
   

}



?>