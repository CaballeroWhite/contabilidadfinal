<?php



class Partida
{
    public $conectarv;

    public $CuentaN;
    public $Cargos;
    public $Abonos;


    public function __construct($CuentaNombre, $CargosPartida, $AbonosPartida)
    {
        $this->conectarv = new conexionPDO();

        $this->CuentaN = $CuentaNombre;
        $this->Cargos = $CargosPartida;
        $this->Abonos = $AbonosPartida;
    }


    public function CargaCuentacatalogo($CuentaN)
    {

        $conexion = $this->conectarv->Conectar();

        $SQL = "INSERT into cuentascatalogo(cargos,abonos) VALUES($this->Cargos,$this->Abonos) WHERE NombreCuenta = ':Cuenta'";
        $statement = $conexion->prepare($SQL);
        $statement->bindParam(':Cuenta',$CuentaN);

        //echo $valoresInsertar;
        if (!$statement) {
            return false;
        } else {

            $statement->execute(); //Inserta el array

            return true;
        }
    }
}
