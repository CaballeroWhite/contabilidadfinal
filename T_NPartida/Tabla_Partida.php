<?php
include '../Conexion/Procesos.php';
include '../sidebar.php';

?>
<!DOCTYPE html>

<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <link href="../assets/css/CustomCss.css" rel="stylesheet">
  <title></title>
</head>

<body>
  <div class="main-panel">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <i class="material-icons">
            <a href="#">arrow_back_ios</a>
          </i>
          <a class="navbar-brand" href="../index.php">Menu</a>
        </div>

        <div class="collapse navbar-collapse justify-content-end">
          <form class="navbar-form">
            <div class="input-group no-border">
            </div>
          </form>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">

                <div>
                  <h4 class="card-title ">Tabla partidas</h4>
                  <p class="card-category">Partidas registradas</p>
                  <br>
                  <i class="material-icons">
                    <a id="md-Border" href="nuevaPartida.php">add</a>
                  </i>
                  <p class="card-category">Nueva partida</p>
                </div>
              </div>

              <form method="POST" >
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">date_range</i>
                      </span>
                    </div>
                    <center>
                    <div>
                    
                      <label for="Fecha">Fecha para filtrar</label>
                      <select class="form-control selectOP" name='Fecha'>
                        <option>--Seleccione la fecha--</option>
                        <?php
                        $campoFecha = array(

                          "Fecha"
                        );
                        $Fecha = new Procesos();

                        $Fechas = $Fecha->ConsultaFechaPartida($campoFecha, "t_partidas");
                        foreach ($Fechas as $Date) {

                          ?>

                          <option><?php echo $Date["Fecha"]; ?></option>
                        <?php } ?>
                      </select>
                    
                    </div>
                      </center>
                  </div>
                </div>
                <center>
                <input class="btn btn-info" type="submit" name="Buscar" value="Buscar">
              
                <a href='ReportePartidas.php?Fecha=<?php echo str_replace("-","",$_POST['Fecha']);?>'><i class='material-icons'>description</i></a>
                </center>

              </form>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>Fecha</th>
                      <th>Concepto</th>
                      <th>Debe</th>
                      <th>Haber</th>

                    </thead>
                    <tr>

                      <?php

                      if (isset($_POST)) {
                        if (isset($_POST['Buscar'])) {
                          $FechaMostrar = $_POST['Fecha'];
                          $PartidasFecha = new Procesos();
//Consulta de la fecha
                          $Partidas = $PartidasFecha->FiltarPartidas($FechaMostrar);
                          
                          echo "<td>".$Partidas[0]['Fecha']."</td>";
                              
                          if($Partidas == null){
                            echo "Datos no encontrados";
                          }else{
                              foreach ($Partidas as $Partida) {
                                echo "<tr></tr>";
                                echo "<td></td>";
                                echo "<td>".$Partida['Concepto']."</td>";
                                echo "<td>".$Partida['Debe']."</td>";
                                echo "<td>".$Partida['Haber']."</td>";
                                echo "</tr>";
                              }
                            }
                            
                            
                        }
                      }

                      ?>
             
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
          </div>
</body>

</html>