<?php

require '../fpdf181/fpdf.php';
require '../Conexion/Procesos.php';

class holapdf extends FPDF
{
    function Header()
    {
        $this->SetFont('Arial', 'B', 15);
        $this->Cell(30);
        $this->Cell(120, 10, 'Reporte De Estados', 0, 0, 'C');
        $this->Ln(20);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 5);
        $this->Cell(0, 10, 'Pagina ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }


    function ImprovedTable($header, $data)
    {
        // Anchuras de las columnas
        $w = array(30, 70, 50, 40);
        // Cabeceras
        for ($i = 0; $i < count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
        $this->Ln();
        // Datos

         $DebeT = 0;
         $HaberT = 0;
        foreach ($data as $row) {
            
            $this->Cell($w[0], 6, $data[0]['Fecha'], 'LR');
            $this->Cell($w[1], 6, $row['Concepto'], 'LR');
            $this->Cell($w[2], 6, $row['Debe'], 'LR', 0, 'R');
            $this->Cell($w[3], 6, $row['Haber'], 'LR', 0, 'R');
           
            
            $this->Ln();
            $DebeT += $row['Debe'];
            $HaberT += $row['Haber'];

            
        }
        $this->Cell(30, 6, "Total", 'LR', 0, 'R');
        $this->Cell(70);
        
        
        $this->Cell(50, 6, $DebeT, 'LR', 0, 'R');
        $this->Cell(40, 6, $HaberT, 'LR', 0, 'R');
        // Línea de cierre
        $this->Cell(array_sum($w), 0, '', 'T');
    }
}

$pdf = new holapdf();

$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFillColor(232, 232, 232);
$pdf->SetFont('Arial', 'B', 9);


// Títulos de las columnas
$header = array('Fecha', 'Concepto', 'Debe', 'Haber');



$PartidasFecha = new Procesos();
//$FechaMostrar = $_GET['Fecha'];
//Consulta de la fecha
$FechaBusqueda = $_GET['Fecha'];
$Partidas = $PartidasFecha->FiltarPartidas($FechaBusqueda);



$pdf->SetFont('Arial', '', 10);

$pdf->ImprovedTable($header,$Partidas);

$pdf->Output();

