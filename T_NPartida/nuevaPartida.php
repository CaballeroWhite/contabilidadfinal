<?php

include '../Conexion/Procesos.php';
include '../sidebar.php';
include 'ClassPartida.php';


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <script src="../js/jquery-3.4.1.min.js">
    </script>


    <meta charset="utf-8">
    <link href="../assets/css/CustomCss.css" rel="stylesheet">
    <title>Nueva Partida</title>
</head>

<body>
    <center>
        <form method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
            <h3 style='color:white;'>Cuantos partidas desea crear:</h3> <input class="form-control cantidadCustom" type="number" name="cantidad">
            <input class="btn btn-info" type="submit" value="Generar" name="generar">
            <center>
                <h3 style='color:white;'>Escriba sus partidas</h3>
            </center>
        </form>


        <div class="content">
            <?php


            $cantidadLineas;
            if ($_POST) {

                if (isset($_POST["generar"])) {
                    $cantidadLineas = $_POST["cantidad"];
                    //setcookie("Lineas",$cantidadLineas);

                    $_SESSION["Lineas"] = $cantidadLineas;
                    echo "
               
               <form  method='post' action='$_SERVER[PHP_SELF]'>
               <div class='col-md-10 col-lg-8 col-sm-12'>
              <div class='card-body'>
               <div class='table-responsive'>
               <table class='table'>
               <thead class='text-primary'>
                <th>#</th>
                <th>Fecha</th>
                <th>#Partida</th>
                <th>Cuenta</th>
                <th>Concepto</th>
                <th>Debe</th>
                <th>Haber</th>
               </thead>
               <tbody>
                <div class='row'>
                <div class='col-md-10'>
                <div class='form-group'>
               ";
                    for ($i = 1; $i <= $cantidadLineas; $i++) {
                        echo "
                   <tr>
                  
                    <td>$i</td>
                    <td><input class='form-control datetimepicker DateCustom' type='date' name='Fecha[]'></td>
                    <td><input class='form-control PartidaCustom' type='number' name='Partida[]' placeholder='# de partida' value='$i'></td>
                    <td><input class='form-control ConceptoCustom' type='text' id='Cuenta$i' name='cuenta[]' onKeyUp='buscar();' placeholder=''></td>
                    <td><input class='form-control ConceptoCustom' type='text' id='Concepto$i' name='Concepto[]'  placeholder='Ingresar concepto'></td>
                    <td><input class='form-control DebeCustom' type='number' step='any' id='Debe$i' name='Debe[]' onKeyUp='SumDebe();' value='0' required></td>
                    <td><input class='form-control HaberCustom' type='number' step='any' id='Haber$i' name='Haber[]' onKeyUp='SumHaber();' value='0' required></td>
                 
                   </tr>
                   ";
                    }
                    echo "
                    
               <tr>
               
                <td colspan='7' align='center'>
                <input class='btn btn-info'type='submit' value='Guardar' name='Finalizar'>
                <input class='form-control'type='text' placeholder='Total Debe' name='DebeCol' id='DebeCol' required>
               
                
                <input class='form-control'type='text' placeholder='Total Haber' name='HaberCol' id='HaberCol' required>
               
                </td>
                    
             
                
                
                
                
                
               </tr>
                     </div>
                       </div>
                       </div>
                       </div>
               </tbody>
               
               </table>
               </div>
               </div>
               </form>
               
               ";
                } else if (isset($_POST["Finalizar"])) {
                    //$DatosListar = $_COOKIE['Lineas'];
                    $DatosListar = $_SESSION["Lineas"];
                    $DatosInsertar;
                    $Campos = array(
                        "Fecha",
                        "NPartida",
                        "Cuenta",
                        "Concepto",
                        "Debe",
                        "Haber"
                    );

                    $PartidaN = new Procesos();



                    //Consulta de datos de Cargos y Abonos para almacenar y luego sumar los datos nuevos
                    //Variables para almacenar la cantidad que tienen los campos actualmente
                    $CargosBD;
                    $AbonosBD;
                    $CamposCatalogo = array(
                        "cargos",
                        "abonos"
                    );

                    for ($j = 0; $j < $DatosListar; $j++) {
                        $DatosCatalogo = $PartidaN->ConsultaCargosAbonos($CamposCatalogo, "cuentascatalogo", $_POST['cuenta'][$j]);
                    }


                    foreach ($DatosCatalogo as $CargosAbonos) {
                        $CargosBD = $CargosAbonos["cargos"];
                        $AbonosBD = $CargosAbonos["abonos"];
                    }

                

                    $CargoNuevo = 0;
                    $AbonoNuevo = 0;
                    for ($j = 0; $j < $DatosListar; $j++) {

                        /*echo $_POST['Fecha'][$j]." ";
                    echo $_POST['Partida'][$j]." ";
                    echo $_POST['cuenta'][$j]." ";
                    echo $_POST['Concepto'][$j]." ";
                    echo $_POST['Debe'][$j]." ";
                    echo $_POST['Haber'][$j]."<br/>";*/

                        $DatosInsertar = array(
                            $_POST['Fecha'][$j],
                            $_POST['Partida'][$j],
                            $_POST['cuenta'][$j],
                            $_POST['Concepto'][$j],
                            $_POST['Debe'][$j],
                            $_POST['Haber'][$j]

                        );

                        //Variables que suman lo obtenido de la consulta de los datos y le agregan los datos del los inputs o los nuevos valores
                        $CargoNuevo = $CargosBD + $_POST['DebeCol'];
                        $AbonoNuevo = $AbonosBD + $_POST['HaberCol'];
                        //------
                        $Dd = $PartidaN->Insertar($Campos, "t_partidas", $DatosInsertar);

                        //Llamada al metodo que carga los campos cargos y abonos, en la tabla cuentascatalogo
                        $Dd = $PartidaN->CargaCuentacatalogo("cuentascatalogo",$CargoNuevo,$AbonoNuevo,$_POST['cuenta'][$j]);
                     

                    }

                   

                    if ($Dd) {
                        echo "Datos Registrados";
                    } else {
                        echo "Error en registro";
                    }
                }
            }



            ?>
        </div>


        <script>
            var IntConcepto = "<?php echo $_SESSION["Lineas"]; ?>";


            function buscar() {
                for (var i = 1; i <= IntConcepto; i++) {

                    var textoBusqueda = $("input#Cuenta" + i).val();
                    /* 
                        “buscar.php” -> Toma la URL donde estará el PHP que va a devolver los resultados de la consulta a MySQL (eso lo haremos un poco más adelante).
                        valorBusqueda: textoBusqueda -> Donde “valorBusqueda” es el nombre del POST que le daremos en PHP ( $_POST[‘valorBusqueda’] ) para obtener lo que hay en “textoBusqueda”, que es el ID del input en el formulario.
                        function(mensaje) -> Crea una función que devolverá el mensaje (el cual no será más que un echo de PHP) en el contenedor con un ID resultadoBusqueda (es decir: success).
                        $(“#resultadoBusqueda”).html(mensaje) -> En el contenedor resultadoBusqueda se mostrará ese echo en PHP.
                    */
                    if (textoBusqueda != "") {
                        $.post("buscar.php", {
                            valorBusqueda: textoBusqueda
                        }, function(mensaje) {
                            $("#Concepto" + parseInt((i - 1))).val(mensaje);

                        });
                    } else {
                        ("#Concepto" + parseInt((i - 1))).val('cuenta no existe');
                    };

                }

            };



            function SumDebe() {
                var DebeTotal = 0;
                for (var i = 1; i <= IntConcepto; i++) {
                    var DebeCantidad = $("#Debe" + i).val();
                    DebeTotal = parseFloat(DebeTotal) + parseFloat(DebeCantidad);

                }
                $("#DebeCol").val(DebeTotal);

            };

            function SumHaber() {

                var HaberTotal = 0;
                for (var j = 1; j <= IntConcepto; j++) {
                    var HaberCantidad = $("#Haber" + j).val();
                    HaberTotal = parseFloat(HaberTotal) + parseFloat(HaberCantidad);

                }
                $("#HaberCol").val(HaberTotal);


            };
        </script>

</body>

</html>