<?php


require '../Conexion/Conexion.php';

class ValidarSesion{
    public $conectarv;

    public function __construct()
    {
        $this->conectarv = new conexionPDO();
    }

    public function Login(){
        
        $conexion = $this->conectarv->Conectar();
        $SQL = 'SELECT * FROM users WHERE nombre_usuario=:UsuarioN AND password = :PassUsuario';
        $sth = $conexion->prepare($SQL);

        $sth->bindParam(":UsuarioN",$_POST['UserN']);
        $sth->bindParam(":PassUsuario",$_POST['PassW']);
        $sth->execute();

        $UsuarioSesion = $sth->fetchAll();

        if($UsuarioSesion){
            session_start();//Iniciar la sesion

            $tipoUsuario = $UsuarioSesion[0]['titulo'];

            switch($tipoUsuario){
                case "Admin":
                        $_SESSION['user'] = $UsuarioSesion[0]['nombre_usuario'];
                        $_SESSION['tipo'] = $UsuarioSesion[0]['titulo'];
                        //Menu del administrador
                        header('location:../../home.php');
                        echo "inicio de sesion como admin";
                        break;
                case "Estandar":
                        $_SESSION['user'] = $UsuarioSesion[0]['nombre_usuario'];
                        $_SESSION['tipo'] = $UsuarioSesion[0]['titulo'];
                        //Menu del usuario estandar
                        header('location:../../home2.php');
                        echo "inicio de sesion como estandar";
                        break;

            }

        }else{
            //Manda a iniciar sesion

            header('location:../../index.php');
            echo '<script language="javascript">';
            echo 'alert("Error en los datos")';
            echo '</script>';
        }

        
    }



}


?>
