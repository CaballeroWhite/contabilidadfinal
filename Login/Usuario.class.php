<?php
require_once "../Conexion/Conexion.php";

class Usuarios
{
 private $DB_con;
 
 function __construct($DB_con)
 {
  $this->db = $DB_con;
 }
 
 public function create($usuario,$pass)
 {
  try
  {
   $stmt = $this->db->prepare("INSERT INTO users(nombre_usuario,password) VALUES(:usuario, :pass)
    ");
   $stmt->bindparam(":usuario",$usuario);
   $stmt->bindparam(":pass",$pass);
   $stmt->execute();
   return true;
  }
  catch(PDOException $e)
  {
   echo $e->getMessage(); 
   return false;
  }
  
 }
 
 public function getID($id)
 {
  $stmt = $this->db->prepare("SELECT * FROM users WHERE id=:id");
  $stmt->execute(array(":id"=>$id));
  $editRow=$stmt->fetch(PDO::FETCH_ASSOC);
  return $editRow;
 }
 
 public function update($id,$usuario,$pass)
 {
  try
  {
   $stmt=$this->db->prepare("UPDATE users SET id=:id, 
                                              nombre_usuario=:usuario,  
                password=:pass
             WHERE id=:id ");
   $stmt->bindparam(":usuario",$usuario);
   $stmt->bindparam(":pass",$pass);
   $stmt->bindparam(":id",$id);
   $stmt->execute();
header("location:Listar.php");

   return true; 
  }
  catch(PDOException $e)
  {
   echo $e->getMessage(); 
   return false;
  }
 }
 
 public function delete($id)
 {
  $stmt = $this->db->prepare("DELETE FROM users WHERE id=:id");
  $stmt->bindparam(":id",$id);
  $stmt->execute();
  return true;
 }
 
 /* tabla */
 
 public function dataview($query)
 {
  $stmt = $this->db->prepare($query);
  $stmt->execute();
 
  if($stmt->rowCount()>0)
  {
   while($row=$stmt->fetch(PDO::FETCH_ASSOC))
   {
    ?>
                <tr>
                <td><?php echo($row['id']); ?></td>
                <td><?php echo($row['usuario']); ?></td>
                <td><?php echo(base64_decode($row['pass'])); ?></td>
                <td align="center">
                <a href="../actualizar.php?edit_id=<?php print($row['id']); ?>"><i class="fas fa-marker"></i></a>
                </td>
                <td align="center">
                <a href="delete.php?delete_id=<?php print($row['id']); ?>"><i class="fas fa-trash-alt"></i></a>
                </td>
                </tr>
                <?php
   }
  }
  else
  {
   ?>
            <tr>
            <td>Sin Datos......</td>
            </tr>
            <?php
  }
  
 }
 public function paging($query,$records_per_page)
 {
  $starting_position=0;
  if(isset($_GET["page_no"]))
  {
   $starting_position=($_GET["page_no"]-1)*$records_per_page;
  }
  $query2=$query." limit $starting_position,$records_per_page";
  return $query2;
 }
 
 public function paginglink($query,$records_per_page)
 {
  
  $self = $_SERVER['PHP_SELF'];
  
  $stmt = $this->db->prepare($query);
  $stmt->execute();
  
  $total_no_of_records = $stmt->rowCount();
  
  if($total_no_of_records > 0)
  {
   ?><ul class="pagination"><?php
   $total_no_of_pages=ceil($total_no_of_records/$records_per_page);
   $current_page=1;
   if(isset($_GET["page_no"]))
   {
    $current_page=$_GET["page_no"];
   }
   if($current_page!=1)
   {
    $previous =$current_page-1;
    echo "<li><a href='".$self."?page_no=1'>Principio</a></li>";
    echo "<li><a href='".$self."?page_no=".$previous."'>Anterior</a></li>";
   }
   for($i=1;$i<=$total_no_of_pages;$i++)
   {
    if($i==$current_page)
    {
     echo "<li><a href='".$self."?page_no=".$i."' style='color:red;'>".$i."</a></li>";
    }
    else
    {
     echo "<li><a href='".$self."?page_no=".$i."'>".$i."</a></li>";
    }
   }
   if($current_page!=$total_no_of_pages)
   {
    $next=$current_page+1;
    echo "<li><a href='".$self."?page_no=".$next."'>Siguiente</a></li>";
    echo "<li><a href='".$self."?page_no=".$total_no_of_pages."'>Anterior</a></li>";
   }
   ?></ul><?php
  }
 }
 
}