<?php
  include ('../sidebar.php');
  //include '../inc/Procesos.php';
?>

<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="../assets/css/CustomCss.css" rel="stylesheet">
    <title></title>
  </head>

  <body>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <i class="material-icons"><a href="T_Categorias_Ingresos.php">arrow_back_ios</a></i><a class="navbar-brand" href="#">Mostrar Categorías de Gastos</a>
          </div>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border"></div>
            </form>
          </div>
        </div>
      </nav>

      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <div><h4 class="card-title ">Nueva Categoría de Gastos</h4><br></div>
                </div>

                <div class="content">
                  <div class="col-md-12">
                    <form method="POST">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Categoría</label>
                            <input type="text" class="form-control" name="nombre" required>
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <label>Fecha</label>
                            <input type="date" class="form-control" name="fecha" required>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <input class="btn btn-primary btn-round" type="submit" name="guardar" value="Guardar">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>

<?php
  $NCategoria = new Procesos();

  if(isset($_POST['guardar'])){
    $Categoria = $_POST['nombre'];
    $Fecha = $_POST['fecha'];

    $Campos = Array(
      "nombre",
      "fecha"
    );
    $Valores = Array(
      $Categoria,
      $Fecha
    );

    echo $NCategoria->Insertar($Campos,"categoria_ingreso",$Valores);
    if($NCategoria){
      echo "Datos Registrados";
    }
    else{
      echo "Error en datos";
    }
  }
?>
