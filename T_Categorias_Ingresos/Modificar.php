<?php
  include '../Conexion/Procesos.php';
  include '../sidebar.php';
  $Categoria = new Procesos();
  $id = $_GET['idD'];
  $CargarDatos = $Categoria->CargaDatos($id,"id_categoria","categoria_ingreso");
?>

<!DOCTYPE html>
<html>
  <head>
    <link href="../assets/css/CustomCss.css" rel="stylesheet">
    <title></title>
  </head>

  <body>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <i class="material-icons"><a href="T_Categorias_Ingresos.php">arrow_back_ios</a></i>
            <a class="navbar-brand" href="#">Actualizar Categoría</a>
          </div>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
              </div>
            </form>
          </div>
        </div>
      </nav>

      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <div><h4 class="card-title ">Actualizar Categoría de Ingreso</h4></div>
                </div>

                <div class="content">
                  <div class="col-md-12">
                    <form method="POST" action="Actualizar.php">
                      <?php
                        foreach($CargarDatos as $Datos){

                      ?>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Categoría</label>
                            <input type="text" class="form-control" name="nombre" value="" required><?php echo $Datos["nombre"]?>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Fecha</label>
                            <input type="date" class="form-control"  name="fecha"  value="<?php echo $Datos["fecha"]?>" required>
                          </div>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $id ?>">

                    <div class="col-md-12">
                      <input class="btn btn-primary btn-round" id="GuardarIngreso" type="submit" name="save" value="Guardar">
                    </div>
                  </form>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </body>
</html>
