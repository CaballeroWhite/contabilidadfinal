<?php
include '../Conexion/Procesos.php';
include '../sidebar.php';

$Cuentas = new Procesos();


$id = $_GET['idD'];
$CargarDatos = $Cuentas->CargaDatos($id,"IdCuenta","cuentascatalogo");


?>
<!DOCTYPE html>

<html>

    <head>
        <link href="../assets/css/CustomCss.css" rel="stylesheet">
        <title>Nuevo ingreso</title>
    </head>
    <body>

    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <i class="material-icons">
                <a href="Tabla_Cuentas.php">arrow_back_ios</a>
            </i>
            <a class="navbar-brand" href="Tabla_Cuentas.php">Cambiar cuenta</a>
          </div>

          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
              </div>
            </form>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  
                  <div>
                    <h4 class="card-title ">Modificar cuenta</h4>
                  </div>
                </div>

                <div class="content">
                  <div class="col-md-12">
                    <form method="POST" action="CuentasUpdate.php">
                    <?php
    
                        foreach($CargarDatos as $Datos){

                    ?>
                      <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="Cuenta">Nombre de cuenta</label>
                                  <textarea id="textDescripcionarea" class="form-control" name="Cuenta"  rows="1" required><?php echo $Datos["NombreCuenta"];?></textarea>
                                </div>
                            </div>
                            <div class="col-md-5">
                              <div class="form-group">
                                  <label for="BalanceI">Balance Inicial</label>
                                  <input type="text" class="form-control" name="BalanceI" value="<?php echo $Datos["saldo_actual"];?>" required>  
                        
                              </div>
                            </div>
                      </div>

          
                      </div>
                      <input type="hidden" name="id" value="<?php echo $id ?>">
                            <div class="col-md-12">
                              <input class="btn btn-primary btn-round" id="GuardarIngreso" type="submit" name="save" value="Guardar">
                            </div>                
                      </form>
                      <?php } ?>
                    </div>

                  </div>
                    </div>
                </div>
              </div>
            </div>
      </div>
    </div>
    </body>


</html>