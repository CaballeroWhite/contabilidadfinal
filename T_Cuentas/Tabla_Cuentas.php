<?php

include ('../sidebar.php');
//include '../inc/Procesos.php';
?>
<!DOCTYPE html>

<html lang="en" dir="ltr"> 

    <head>
        <meta charset="utf-8">
        <link href="../assets/css/CustomCss.css" rel="stylesheet">
        <title></title>
    </head>

    <body>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <i class="material-icons">
                <a href="#">arrow_back_ios</a>
            </i>
            <a class="navbar-brand" href="#">Registro de cuentas</a>
          </div>

          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
              </div>
            </form>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  
                  <div>
                    <h4 class="card-title ">Registro de cuentas</h4>
                    <p class="card-category"> Datos de cuentas</p>
                    <br>
                    <i class="material-icons">
                      <a id="md-Border" href="Nueva_Cuenta.php">add</a>
                    </i>
                    <p class="card-category">Nueva cuenta</p>
                  </div>
                </div>

                <form method="POST">
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">group</i>
                            </span>
                        </div>

                            <input class="form-control" type="text" name="NCuenta" placeholder="Nombre de cuenta">
                    </div>
                    </div>
                    <center>
                    <input class="btn btn-primary" type="submit" name="listar" value="Mostrar Todo">
                    <input class="btn btn-info" type="submit" name="Buscar" value="Buscar">
                    </center>
                   
                </form>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>Codigo Cuenta</th>
                        <th>Nombre cuenta</th>
                        <th>Saldo Actual</th>
                      </thead>
                      <tbody>
                        <tr>
                        <?php
            
            include '../Conexion/Procesos.php';


            if(isset($_POST['Buscar'])){

                $Ingresos = new Procesos();
                $DatosB = $Ingresos->BuscarDatos($_POST['NCuenta'],"NombreCuenta","cuentascatalogo");


                if(!$DatosB){
                    echo "Datos no encontrados";
                }else{

                    foreach($DatosB as $fila){
                        echo "<tr>";
                        echo "<td>".$fila['CodigoCuenta']."</td>";
                        echo "<td>".$fila['NombreCuenta']."</td>";
                       
                        
                        echo "<td>".$fila['saldo_actual']."</td>";
                     
                        //valores en eliminar tabla = nombre de la tabla ejempo -> locales_medicos
                        // idCampo = el nombre del campo del id en la tabla ejemplo -> id_Local
                        // id= al campo en la tabla ejemplo -> id_local
        
                        echo "<td><a href='Eliminar.php?tabla=cuentas&idCampo=idCuenta&id=".$fila['idCuenta']."'><i class='material-icons'>delete_sweep</i></a></td>";
                        echo "<td><a href='Formulario_Modificar.php?idD=".$fila['idCuenta']."'><i class='material-icons'>update</i></a></td>";
                        echo "</tr>";
    
    
                    }

                }


            }else if(isset($_POST['listar'])){
                Listar();
            }else{
                Listar();
            }

            function Listar(){
                $Ingresos = new Procesos();
                $Campos = array(
                    "idCuenta",
                    "CodigoCuenta",
                    "NombreCuenta",
                    "tipo_movimiento",
                    "saldo_anterior",
                    "saldo_actual",
                    "cargos",
                    "abonos"
                    
                );
                $Datos = $Ingresos->Consulta($Campos,"cuentascatalogo");

                if(!$Datos){
                    echo "Sin registros";
                }else{


                      foreach($Datos as $fila){
                        echo "<tr>";
                        echo "<td>".$fila['CodigoCuenta']."</td>";
                        echo "<td>".$fila['NombreCuenta']."</td>";
                      
                        echo "<td>".$fila['saldo_actual']."</td>";
                        
                        //valores en eliminar 
                        //tabla = nombre de la tabla, ejempo -> ingresos
                        // idCampo = el nombre del campo del id en la tabla, ejemplo -> id
                        // id = al valor para identificar el registro del campo en la tabla, ejemplo -> id
                        echo "<td><a href='Eliminar.php?tabla=cuentas&idCampo=idCuenta&id=".$fila['idCuenta']."'><i class='material-icons'>delete_sweep</i></a></td>";
                        echo "<td><a href='Formulario_Modificar.php?idD=".$fila['idCuenta']."'><i class='material-icons'>update</i></a></td>";
                        
                        echo "</tr>";


                    }

                }



            }          
            ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
    </body>

</html>